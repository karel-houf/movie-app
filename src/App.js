import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Home from "./components/Home/Home";
import Navbar from "./components/layout/Navbar";
import Footer from "./components/layout/Footer";
import TitleDetail from "./components/Movies/TitleDetail";

import { Provider } from './utils/context';

require('./styles/main.sass');
 
const App = () => {
  return (
    <Provider>
      <Router>
      <div className="container">
        <Navbar />

        <Route exact path="/" component={Home} />
        <Route path="/title/:id" component={TitleDetail} />

        <Footer />
      </div>
    </Router>
    </Provider>
  );
}

export default App;
