const TMDB_KEY = '479a22252cdc74babfe4e1c9e2812dc0';
const OMDB_KEY = '4dda0a58';

export default {
  TMDB_KEY: TMDB_KEY,
  OMDB_KEY: OMDB_KEY,
  popularList: `https://api.themoviedb.org/3/movie/popular?api_key=${TMDB_KEY}&language=en-US&page=1`,
  upcommingList: `https://api.themoviedb.org/3/movie/upcoming?api_key=${TMDB_KEY}&language=en-US&page=1`,
  data: `http://www.omdbapi.com/?apikey=${OMDB_KEY}&`
}
