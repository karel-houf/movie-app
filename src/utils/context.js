import React, { Component } from "react";
import axios from 'axios';

import API from "./api";

const Context = React.createContext();

const reducer = (state, action) => {
  switch (action.type) {
    case 'SEARCH_MOVIES':
      return {
        ...state,
        search_results: action.payload,
        heading: 'Search Results'
      }
    default:
      return state
  }
}

export class Provider extends Component {
  state = {
    top_movie_list: [],
    upcoming_movie_list: [],
    search_results: [],
    heading: 'TOP Movies',
    dispatch: action => this.setState(state => reducer(state, action))
  }

  // Get list of popular movies from the API and set them as state
  getPopularList = () => {
    axios
      .get(API.popularList)
      .then(res => {
        this.setState({ top_movie_list: res.data.results });
      })
      .catch(console.log);
  }

  // Get list of upcomming movies from the API and set them as state
  getUpcommingList = () => {
    axios
      .get(API.upcommingList)
      .then(res => {
        this.setState({ upcoming_movie_list: res.data.results });
      })
      .catch(console.log);
  }
  
  componentDidMount() {
    this.getPopularList();
    this.getUpcommingList();
  }

  render() {   
    return (
      <Context.Provider value={this.state}>
        { this.props.children }
      </Context.Provider>
    );
  }
}

export const Consumer = Context.Consumer;
