import React from "react";
import { Consumer } from "../../utils/context";

import Movies from "../Movies/Movies";
import SearchResults from "../Search/SearchResults";
import Search from "../Search/Search";

const Home = () => {
  return (
    <Consumer>
      {value => {
        const { search_results } = value;

        // If there are no search results render "main page" with movie lists
        // else render the search results
        return (
          <React.Fragment>
            <div className="content">
              <Search />
              { // Render Movies or SearchResults based on if user searched or not
                search_results === undefined ||
                search_results.length === 0
                  ? <Movies />
                  : <SearchResults />
              }
            </div>
          </React.Fragment>
        );

      }}
    </Consumer>
  );
};

export default Home;
