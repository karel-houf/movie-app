import React, { Component } from "react";
import { Consumer } from "../../utils/context";
import axios from "axios";

import API from "../../utils/api";

class Search extends Component {
  state = {
    search_query: "",
    search_results: []
  };

  // Set whatever user types into search bar to state
  onInputChage = event => {
    // event.target.name = name param on input needs to be same as state
    this.setState({ [event.target.name]: event.target.value });
  };

  onFormSubmit = (dispatch, event) => {
    event.preventDefault();

    // Call API for user movie search
    axios
      .get(`https://api.themoviedb.org/3/search/movie?api_key=${API.TMDB_KEY}&language=en-US&query=${this.state.search_query}&page=1&include_adult=false`)
      .then(res => {
        // Send dispatch to provider and tell it that aciton type is SEARCH_MOVIES
        // so thhat reducer can send correct "global state" as search results
        dispatch({
          type: 'SEARCH_MOVIES',
          payload: res.data.results
        });

        this.setState({ search_query: '' });
      })
      .catch(console.log);
  }

  render() {
    return (
      <Consumer>
        {value => {
          const { dispatch } = value;

          return (
            <div className="search">
              <form
                onSubmit={this.onFormSubmit.bind(this, dispatch)}
                className="search__form"
              >
                <input
                  type="text"
                  placeholder="Movie title..."
                  name="search_query"
                  value={this.state.search_query}
                  onChange={this.onInputChage}
                  className="search__input"
                />
                <button type="submit" className="search__button">Search</button>
              </form>
            </div>
          )
        }}
      </Consumer>
    );
  }
}

export default Search;
