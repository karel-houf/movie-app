import React, { Component } from 'react'
import { Consumer } from "../../utils/context";

import Title from "../Movies/Title";

class SearchResults extends Component {
  onHomeClick = () => {
    window.location.reload();
  }

  render() {
    return (
      <Consumer>
        {value => {
          const { search_results, heading } = value;

          if (search_results === undefined || search_results.length === 0) {
            return <h1>Loading...</h1>
          } else {
            return (
              <React.Fragment>
                <div className="results__home">
                  <p className="results__link" onClick={this.onHomeClick}>Home</p>
                </div>
                <div className="list">
                  <h3 className="list__title">{heading}</h3>
                  <div className="list__container">
                    {search_results.map(movie => (
                      <Title key={movie.id} movie={movie} />
                    ))}
                  </div>
                </div>
              </React.Fragment>
            );
          }
        }}
      </Consumer>
    );
  }
}

export default SearchResults
