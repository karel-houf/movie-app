import React from "react";

import Title from "./Title";

const TitleList = ({ data }) => {
  return (
    <React.Fragment>
      <div className="list">
        <h3 className="list__title">Popular Movies</h3>
        <div className="list__container">
          {
            data.slice(0, 10).map(movie => (
              <Title key={movie.id} movie={movie} />
            ))
          }
        </div>
      </div>
    </React.Fragment>
  )
}

export default TitleList;
