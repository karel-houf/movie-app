import React from "react";
import { Link } from "react-router-dom";

const Title = ({ movie }) => {
  return (
    <div className="title">
      <Link to={`/title/${movie.id}`}>
        <img
          className="title__image"
          src={"https://image.tmdb.org/t/p/w300/" + movie.poster_path}
          alt={movie.title + " poster"}
        />
        <div className="title__overlay">
          <span className="title__name">{movie.title}</span>
        </div>
      </Link>
    </div>
  );
};

export default Title;
