import React, { Component } from "react";
import { Link } from "react-router-dom";

import axios from "axios";
import API from "../../utils/api";

class TitleDetail extends Component {
  state = {
    details: {}
  };
  vote_average;

  componentDidMount() {
    // Get details for specific move by id from url /title/:id
    axios
      .get(
        `https://api.themoviedb.org/3/movie/${
          this.props.match.params.id
        }?api_key=${API.TMDB_KEY}&language=en-US`
      )
      .then(res => {
        this.setState({ details: res.data });
      })
      .catch(console.log);
  }

  // Decide what color will be border based on review value
  borderColorOnVote = value => {
    let borderColor = "";

    if (value.vote_average >= 7) {
      // Green
      borderColor = "#00b300";
    } else if (value.vote_average <= 4) {
      // Red
      borderColor = "#f00";
    } else {
      // Orange
      borderColor = "#f90";
    }

    return borderColor;
  };

  render() {
    const { details } = this.state;
    const backdrop = {
      display: "block",
      backgroundSize: "100% 100%",
      zIndex: "-1",
      backgroundImage: `url(http://image.tmdb.org/t/p/original${
        details.backdrop_path
      })`
    };

    const colorOnVote = {
      borderColor: this.borderColorOnVote(details)
    };

    let genreList = [];
    if (details.genres !== undefined) {
      details.genres.map(genre => {
        return genreList.push(genre.name);
      });
    }

    return (
      <React.Fragment>
        <div className="details" style={backdrop}>
          <div className="details__wrapper">
            <div className="details__link">
              <Link to="/" >
                Go back
              </Link>
            </div>
            <img
              src={`https://image.tmdb.org/t/p/w300/${details.poster_path}`}
              alt={`${details.title} poster`}
              className="details__poster"
            />
            <div className="details__main">
              <div className="main__container">
                <div className="main__vote" style={colorOnVote}>
                  <span>
                    {details.vote_average !== undefined
                      ? details.vote_average !== 0
                        ? `${details.vote_average * 10}%`
                        : "NR"
                      : null}
                  </span>
                </div>
                <div className="main__header">
                  <h2 className="main__title">{details.title} </h2>
                  <span className="main__release">
                    {details.release_date !== undefined
                      ? `(${details.release_date.substring(0, 4)})`
                      : null}
                  </span>
                  <p className="main__tag">
                    <i>{details.tagline}</i>
                  </p>
                </div>
              </div>

              <div className="main__content">
                <p className="main__item">{genreList.join(", ")}</p>
                <p className="main__item">
                  <strong className="details__subtitle">Status: </strong>
                  {details.status}
                </p>

                <p className="main__item">
                  <strong className="details__subtitle">Runtime: </strong>
                  {details.runtime} minutes
                </p>

                <p className="main__item">
                  <strong className="details__subtitle">Budget: </strong>
                  {details.budget !== undefined
                    ? details.budget
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    : null}{" "}
                  USD
                </p>

                <p className="main__item">
                  <strong className="details__subtitle">Revenue: </strong>
                  {details.revenue !== undefined
                    ? details.revenue
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    : null}{" "}
                  USD
                </p>

                <p className="main__item">
                  <strong className="details__subtitle">Profit: </strong>
                  {details.revenue !== undefined && details.budget
                    ? (details.revenue - details.budget)
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                    : null}{" "}
                  USD
                </p>
              </div>
            </div>

            <div className="details__overview">
              <p className="overview__title">
                <strong className="overview__bold">Overview: </strong>
                <br />
                {details.overview}
              </p>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default TitleDetail;
