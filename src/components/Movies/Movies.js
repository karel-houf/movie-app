import React, { Component } from "react";
import { Consumer } from "../../utils/context";

import TitleList from "./TitleList";

export class Movies extends Component {
  render() {
    return (
      <Consumer>
        { value => {
          const { top_movie_list, upcoming_movie_list } = value;

          if ((top_movie_list !== undefined || top_movie_list.length !== 0 ) &&
            (upcoming_movie_list !== undefined || upcoming_movie_list.length !== 0)) {
            return(
              <React.Fragment>
                <TitleList data={top_movie_list} />
                <TitleList data={upcoming_movie_list} />
              </React.Fragment>
            )
          } else {
            return <h1>Loading...</h1>;
          }

        }}
      </Consumer>
    );
  }
}

export default Movies;
