import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="navbar">
      <Link to="/" className="navbar__link">
        <h1 className="navbar__title">Movie App</h1>
      </Link>
    </nav>
  );
};

export default Navbar;
