import React from "react";

const Footer = () => {
  return (
    <footer className="footer">
      <p className="footer__copy">2018 &copy; Karel-František Houf</p>
    </footer>
  );
};

export default Footer;